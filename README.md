# vscodium-deb-rpm-repo

Always up-to-date [VSCodium](https://github.com/VSCodium/vscodium) repository

[![Daily Update Status](https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/badges/master/pipeline.svg)](https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/commits/master)

## How to install for **Debian/Ubuntu/Linux Mint**

Add my key:
- `wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg | sudo apt-key add -`
 
Add the repository:
- `echo 'deb https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list`

Update then install vscodium:
- `sudo apt update`
- `sudo apt install vscodium`

Then search for vscodium and run it (e.g. the Activities menu from the Gnome Panel, or whatever else you use as your launcher or application manager) 

## How to install for other distros

#### The metadata and packages of each repository are signed - for correct work you also need to add my GPG key:

- `rpm --import https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/master/pub.gpg`

### Add the repository:

- **Fedora/RHEL**: `dnf config-manager --add-repo https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/rpms/`

- **openSUSE/SUSE**: `zypper addrepo -t YUM https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/raw/repos/rpms/ vscodium_mirror_on_gitlab`

### Then type `vscodium` in [GNOME Software](https://wiki.gnome.org/Apps/Software) or use your package manager:

- `dnf install vscodium`
- `zypper in vscodium`

## Verification

Checksum verification doesn't works 'cause GPG signature change the size of package.
You can use `diff -r` for extracted packages or [pkgdiff](https://github.com/lvc/pkgdiff).

## Updates?

[Every](https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/commits/repos) 24 hours at 03:00 UTC.


## Packages for another Linux distributions

Just make a pull request.